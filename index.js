const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const morgan = require('morgan');
const registerUser = require('./controllers/auth/registerUser');
const loginUser = require('./controllers/auth/loginUser');
const getMyInfo = require('./controllers/user/getMyInfo');
const deleteMyInfo = require('./controllers/user/deleteMyInfo');
const changePassword = require('./controllers/user/changePassword');
const getAllTrucks = require('./controllers/truck/getAllTrucks');
const addNewTruck = require('./controllers/truck/addNewTruck');
const findTruck = require('./controllers/truck/findTruck');
const updateTruck = require('./controllers/truck/updateTruck');
const removeTruck = require('./controllers/truck/removeTruck');
const assignTruck = require('./controllers/truck/assignTruck');
const addNewLoad = require('./controllers/load/addNewLoad');
const changeLoadState = require('./controllers/load/changeLoadState');
const findLoad = require('./controllers/load/findLoad');
const updateLoad = require('./controllers/load/updateLoad');
const deleteLoad = require('./controllers/load/deleteLoad');
const postLoad = require('./controllers/load/postLoad');
const getLoads = require('./controllers/load/getLoads');
const dotenv = require('dotenv');

dotenv.config();

mongoose.connect(
  process.env.MONGO_URI,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    autoIndex: true,
  },
  (err) => {
    if (err) throw new Error(err);
    console.log('MONGO DB connected successfully');
  }
);

const app = express();

const PORT = process.env.PORT || 8080;

app.use(express.json());
app.use(cors());
app.use(morgan('tiny'));

app.route('/api/auth/register').post(registerUser);
app.route('/api/auth/login').post(loginUser);

app.route('/api/users/me').get(getMyInfo).delete(deleteMyInfo);
app.route('/api/users/me/password').patch(changePassword);

app.route('/api/trucks').get(getAllTrucks).post(addNewTruck);
app
  .route('/api/trucks/:id')
  .get(findTruck)
  .put(updateTruck)
  .delete(removeTruck);
app.route('/api/trucks/:id/assign').post(assignTruck);

app.route('/api/loads').get(getLoads).post(addNewLoad);
app.route('/api/loads/active/state').patch(changeLoadState);
app.route('/api/loads/:id').get(findLoad).put(updateLoad).delete(deleteLoad);
app.route('/api/loads/:id/post').post(postLoad);

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

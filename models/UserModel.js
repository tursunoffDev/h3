const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
  role: String,
  email: String,
  password: String,
  created_date: Date,
});

module.exports = mongoose.model('UserModel', UserSchema);

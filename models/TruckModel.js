const mongoose = require('mongoose');

const TruckSchema = mongoose.Schema({
  created_by: String,
  assigned_to: String,
  type: String,
  status: String,
  created_date: Date,
});

module.exports = mongoose.model('TruckModel', TruckSchema);

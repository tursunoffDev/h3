const mongoose = require('mongoose');

const LoadSchema = mongoose.Schema({
  created_by: String,
  assigned_to: String,
  status: String,
  state: String,
  name: String,
  payload: Number,
  pickup_address: String,
  delivery_address: String,
  dimensions: { width: Number, length: Number, height: Number },
  logs: [],
  created_date: Date,
});

module.exports = mongoose.model('LoadModel', LoadSchema);

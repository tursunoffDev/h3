const TruckModel = require('../../models/TruckModel');
const jwt = require('jsonwebtoken');

const assignTruck = (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    TruckModel.findOne(
      {
        created_by: myData._id,
        assigned_to: myData._id,
        status: 'OL',
      },
      (err, truckOnLoad) => {
        if (err) console.log(err);
        else if (truckOnLoad) {
          res.status(400).json({ message: 'Error occured' });
        } else {
          TruckModel.findOneAndUpdate(
            {
              created_by: myData._id,
              assigned_to: myData._id,
              status: 'IS',
            },
            { assigned_to: null },
            (err) => {
              if (err) {
                res.status(500).json({ message: 'Error occured' });
              } else {
                TruckModel.findOneAndUpdate(
                  {
                    _id: id,
                    created_by: myData._id,
                  },
                  { assigned_to: myData._id },
                  { new: true },
                  (err, truck) => {
                    if (err) res.status(500).json({ message: 'Error occured' });
                    else {
                      if (!truck) {
                        res.status(400).json({ message: 'Error occured' });
                      } else {
                        res.status(200).json({
                          message: 'Truck assigned successfully',
                        });
                      }
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  }
};

module.exports = assignTruck;

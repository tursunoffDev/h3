const TruckModel = require('../../models/TruckModel');
const jwt = require('jsonwebtoken');

const findTruck = async (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    TruckModel.findOne(
      {
        _id: id,
        created_by: myData._id,
      },
      (err, truck) => {
        if (err) {
          res.status(500).json({ message: 'Error occured' });
        } else {
          if (!truck) {
            res.status(400).json({ message: 'Error occured' });
          } else {
            const { _id, created_by, assigned_to, type, status, created_date } =
              truck;
            res.status(200).json({
              truck: {
                _id,
                created_by,
                assigned_to,
                type,
                status,
                created_date,
              },
            });
          }
        }
      }
    );
  }
};

module.exports = findTruck;

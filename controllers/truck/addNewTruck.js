const jwt = require('jsonwebtoken');
const TruckModel = require('../../models/TruckModel');

const addNewTruck = (req, res) => {
  const [, token] = req.headers['authorization'].split(' ');
  const { type } = req.body;
  if (!type || !token) {
    res.status(400).json({ message: 'Error occured' });
  } else if (
    type !== 'SPRINTER' &&
    type !== 'SMALL STRAIGHT' &&
    type !== 'LARGE STRAIGHT'
  ) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    if (myData.role !== 'DRIVER') {
      res.status(400).json({ message: 'Error occured' });
    } else {
      TruckModel.create(
        {
          created_by: myData._id,
          assigned_to: null,
          type,
          status: 'IS',
          created_date: new Date(),
        },
        (err) => {
          if (err) {
            res.status(500).json({ message: 'Error occured' });
          } else {
            
            res.status(200).json({
              message: 'Truck created successfully',
            });
          }
        }
      );
    }
  }
};

module.exports = addNewTruck;

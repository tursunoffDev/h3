const TruckModel = require('../../models/TruckModel');
const jwt = require('jsonwebtoken');

const updateTruck = (req, res) => {
  const { type } = req.body;
  const { id } = req.params;
  const [, token] = req.headers['authorization'].split(' ');
  if (type || token) {
    res.status(400).json({ message: 'Error occured' });
  } else if (
    type !== 'LARGE STRAIGHT' &&
    type !== 'SMALL STRAIGHT' &&
    type !== 'SPRINTER'
  ) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secre');
    TruckModel.findOneAndUpdate(
      {
        _id: id,
        created_by: myData._id,
        assigned_to: null,
        status: 'IS',
      },
      { type: type },
      (err, truck) => {
        if (err) {
          res.status(500).json({ message: 'Error occured' });
        } else {
          if (!truck) {
            res.status(400).json({ message: 'Error occured' });
          } else {
            res.status(200).json({
              message: 'Truck details changed successfully',
            });
          }
        }
      }
    );
  }
};

module.exports = updateTruck;

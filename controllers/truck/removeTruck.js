const TruckModel = require('../../models/TruckModel');
const jwt = require('jsonwebtoken');
const UserModel = require('../../models/UserModel');

const removeTruck = (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    TruckModel.findOneAndDelete(
      { _id: id, created_by: myData._id, status: 'IS', assigned_to: null },
      (err, truck) => {
        if (err) {
          res.status(500).json({ message: 'Error occured' });
        } else {
          if (!truck) {
            res.status(400).json({ message: 'Error occured' });
          } else {
            if (truck.assigned_to) {
              UserModel.findByIdAndUpdate(truck.assigned_to, {
                assigned_to: null,
              });
            }
            res.status(200).json({
              message: 'Truck deleted successfully',
            });
          }
        }
      }
    );
  }
};

module.exports = removeTruck;

const jwt = require('jsonwebtoken');
const TruckModel = require('../../models/TruckModel');

const getAllTrucks = (req, res) => {
  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    console.log(chalk.red('No token received!'));
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    if (myData.role !== 'DRIVER') {
      console.log(chalk.red('Unauthorized'));
      res.status(400).json({ message: 'Error occured' });
    } else {
      TruckModel.find({ created_by: myData._id }, (err, trucks) => {
        if (err) res.status(500).json({ message: 'Error occured' });
        else {
          
          res.status(200).json({
            trucks: trucks.map((truck) => {
              return {
                _id: truck._id,
                created_by: truck.created_by,
                assigned_to: truck.assigned_to,
                type: truck.type,
                status: truck.status,
                created_date: truck.created_date,
              };
            }),
          });
          // }
        }
      });
    }
  }
};

module.exports = getAllTrucks;

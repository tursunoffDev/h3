const UserModel = require('../../models/UserModel');
const bcrypt = require('bcryptjs');

const registerUser = (req, res) => {
  const { role, password, email } = req.body;
  if (!role || !password || !email) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    UserModel.findOne({ email }, async (err, user) => {
      if (err) {
        res.status(500).json({ message: 'Error occured' });
      } else {
        if (user == null) {
          UserModel.create(
            {
              email,
              password: await bcrypt.hash(password, 10),
              role,
              created_date: new Date(),
            },
            (err, user) => {
              if (err) {
                res.status(500).json({ message: 'Error occured' });
              } else {
                res.status(200).json({
                  message: 'Profile created successfully',
                });
              }
            }
          );
        } else {
          res.status(400).json({ message: 'Error occured' });
        }
      }
    });
  }
};

module.exports = registerUser;

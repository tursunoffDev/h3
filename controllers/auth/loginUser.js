const UserModel = require('../../models/UserModel');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const loginUser = (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    UserModel.findOne({ email }, async (err, user) => {
      if (err) {
        res.status(500).json({ message: 'Error occured' });
      } else {
        if (user == null || !(await bcrypt.compare(password, user.password))) {
          res.status(400).json({ message: 'Error occured' });
        } else {
          const token = jwt.sign(
            {
              email,
              _id: user._id,
              created_date: user.created_date,
              role: user.role,
            },
            'secret'
          );
          res.status(200).json({ jwt_token: token });
        }
      }
    });
  }
};

module.exports = loginUser;

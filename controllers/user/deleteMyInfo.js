const jwt = require('jsonwebtoken');
const UserModel = require('../../models/UserModel');
const TruckModel = require('../../models/TruckModel');

const deleteMyInfo = (req, res) => {
  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    TruckModel.findOne(
      { status: 'OL', created_by: myData._id },
      (err, truck) => {
        if (err) res.status(500).json({ message: 'Error occured' });
        else if (truck) {
          res.status(400).json({ message: 'Error occured' });
        } else {
          UserModel.deleteOne({ email: myData.email }, (err) => {
            if (err) res.status(500).json({ message: 'Error occured' });
            else {
              res.status(200).json({
                message: 'Profile deleted successfully',
              });
            }
          });
        }
      }
    );
  }
};

module.exports = deleteMyInfo;

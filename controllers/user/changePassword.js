const UserModel = require('../../models/UserModel');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const changePassword = (req, res, next) => {
  const [, token] = req.headers['authorization'].split(' ');
  const { oldPassword, newPassword } = req.body;

  if (!token) {
    res.status(400).json({ message: 'Token invalid occured' });
  } else {
    const { _id } = jwt.verify(token, 'secret');

    UserModel.findById(_id, async (err, user) => {
      if (err) res.status(500).json({ message: 'Error occured' });
      else {
        const passwordsMatch = await bcrypt.compare(oldPassword, user.password);

        if (!passwordsMatch) {
          res.status(400).json('passwords do not match');
        }

        const newHashedPassword = await bcrypt.hash(newPassword, 10);

        await UserModel.findByIdAndUpdate(user._id, {
          password: newHashedPassword,
        });

        res.json({ message: 'Password changed successfully' });
      }
    });
  }
};

module.exports = changePassword;

const jwt = require('jsonwebtoken');
const UserModel = require('../../models/UserModel');

const getMyInfo = (req, res) => {
  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    UserModel.findById(myData._id, (err, user) => {
      if (err) res.status(500).json({ message: 'Error occured' });
      else {
        if (!user) {
          res.status(400).json({ message: 'Error occured' });
        } else {
          res.status(200).json({
            user: {
              _id: myData._id,
              role: myData.role,
              email: myData.email,
              created_date: myData.created_date,
            },
          });
        }
      }
    });
  }
};

module.exports = getMyInfo;

const LoadModel = require('../../models/LoadModel');
const jwt = require('jsonwebtoken');

const updateLoad = (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers['authorization'].split(' ');
  const fields = [
    'name',
    'payload',
    'pickup_address',
    'delivery_address',
    'dimensions',
  ];
  const dimensions = ['width', 'length', 'height'];
  let hasAllFields = true;
  fields.forEach((field) => {
    if (!req.body[field]) {
      hasAllFields = false;
    } else if (field === 'dimensions' && req.body[field]) {
      dimensions.forEach((dimension) => {
        if (!req.body[field][dimension]) {
          hasAllFields = false;
        }
      });
    }
  });
  if (!hasAllFields || !token) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    if (myData.role !== 'SHIPPER') {
      res.status(400).json({ message: 'Error occured' });
    } else {
      LoadModel.update(
        {
          _id: id,
          created_by: myData._id,
          status: 'NEW',
        },
        {
          name: req.body.name,
          payload: req.body.payload,
          pickup_address: req.body.pickup_address,
          delivery_address: req.body.delivery_address,
          dimensions: req.body.dimensions,
        },
        { new: true },
        (err, load) => {
          if (err) {
            res.status(500).json({ message: 'Error occured' });
          } else {
            if (!load) {
              res.status(400).json({ message: 'Error occured' });
            } else {
              res.status(200).json({
                message: 'Load details changed successfully',
              });
            }
          }
        }
      );
    }
  }
};

module.exports = updateLoad;

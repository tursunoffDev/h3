const LoadModel = require('../../models/LoadModel');
const jwt = require('jsonwebtoken');

const deleteLoad = (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    if (myData.role !== 'SHIPPER') {
      res.status(400).json({ message: 'Error occured' });
    } else {
      LoadModel.findOneAndDelete(
        { _id: id, created_by: myData._id, status: 'NEW' },
        (err, load) => {
          if (err) {
            res.status(500).json({ message: 'Error occured' });
          } else {
            if (!load) {
             
              res.status(400).json({ message: 'Error occured' });
            } else {
              res.status(200).json({
                message: 'Load deleted successfully',
              });
            }
          }
        }
      );
    }
  }
};

module.exports = deleteLoad;

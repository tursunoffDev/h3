const jwt = require('jsonwebtoken');
const LoadModel = require('../../models/LoadModel');

const addNewLoad = (req, res) => {
  const [, token] = req.headers['authorization'].split(' ');
  const fields = [
    'name',
    'payload',
    'pickup_address',
    'delivery_address',
    'dimensions',
  ];
  const dimensions = ['width', 'length', 'height'];
  let hasAllFields = true;
  fields.forEach((field) => {
    if (!req.body[field]) {
      hasAllFields = false;
    } else if (req.body[field]) {
      dimensions.forEach((dimension) => {
        if (!req.body[field][dimension]) {
          hasAllFields = false;
        }
      });
    }
  });
  if (!token) {
    res.status(400).json({ message: 'Error occured' });
  } else if (hasAllFields) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    if (myData.role !== 'SHIPPER') {
      res.status(400).json({ message: 'Error occured' });
    } else {
      LoadModel.create(
        {
          created_by: myData._id,
          assigned_to: null,
          status: 'NEW',
          state: 'Uploaded',
          name: req.body.name,
          payload: req.body.payload,
          pickup_address: req.body['pickup_address'],
          delivery_address: req.body['delivery_address'],
          dimensions: req.body.dimensions,
          logs: [
            {
              message: 'Load created',
              time: new Date(),
            },
          ],
          created_date: new Date(),
        },
        (err, load) => {
          if (err) {
            res.status(500).json({ message: 'Error occured' });
          } else {
            res.status(200).json({
              message: 'Load created successfully',
            });
          }
        }
      );
    }
  }
};

module.exports = addNewLoad;

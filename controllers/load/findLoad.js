const LoadModel = require('../../models/LoadModel');
const jwt = require('jsonwebtoken');

const findLoad = async (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    LoadModel.findOne(
      {
        _id: id,
      },
      (err, load) => {
        if (err) {
          res.status(500).json({ message: 'Error occured' });
        } else {
          if (!load) {
            res.status(400).json({ message: 'Error occured' });
          } else if (
            myData._id !== load.created_by &&
            myData._id !== load.assigned_to
          ) {
            res.status(400).json({ message: 'Error occured' });
          } else {
            res.status(200).json({
              load: {
                _id: load._id,
                created_by: load.created_by,
                assigned_to: load.assigned_to,
                status: load.status,
                state: load.state,
                name: load.name,
                payload: load.payload,
                pickup_address: load.pickup_address,
                delivery_address: load.delivery_address,
                dimensions: load.dimensions,
                logs: load.logs,
                created_date: load.created_date,
              },
            });
          }
        }
      }
    );
  }
};

module.exports = findLoad;

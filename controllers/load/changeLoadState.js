const jwt = require('jsonwebtoken');
const LoadModel = require('../../models/LoadModel');
const TruckModel = require('../../models/TruckModel');
const UserModel = require('../../models/UserModel');

const changeLoadState = (req, res) => {
  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    if (myData.role !== 'DRIVER') {
      res.status(400).json({ message: 'Error occured' });
    } else {
      LoadModel.findByIdAndUpdate(
        {
          assigned_to: myData._id,
          status: 'ASSIGNED',
        },
        (err, load) => {
          if (err) {
            res.status(500).json({ message: 'Error occured' });
          } else if (!load) {
            res.status(400).json({ message: 'Error occured' });
          } else {
            if (load.state === 'Arrived to delivery') {
              console.log(
              );
              res.status(400).json({ message: 'Error occured' });
            } else {
              const nextState = (function (load) {
                const states = [
                  'En route to Pick Up',
                  'Arrived to Pick Up',
                  'En route to delivery',
                  'Arrived to delivery',
                ];
                for (let i = 0; i < states.length; i++) {
                  if (states[i] === load.state) {
                    return states[i + 1];
                  }
                }
              })(load);
              if (nextState === 'Arrived to delivery') {
                load.status = 'SHIPPED';
                UserModel.findOne({ _id: load.assigned_to }, (err, user) => {
                  if (err) console.log(err);
                  else {
                    TruckModel.findOneAndUpdate(
                      { created_by: user._id },
                      { assigned_to: null, status: 'IS' },
                      (err, truck) => {
                       
                      }
                    );
                  }
                });
              }
              load.state = nextState;
              load.logs = [
                ...load.logs,
                {
                  message: `Load state changed to '${nextState}'`,
                  time: new Date(),
                },
              ];
              load.save((err) => {
                if (err) res.status(500).json({ message: 'Error occured' });
                else {
                 
                  res.status(200).json({
                    message: `Load state changed to '${nextState}'`,
                  });
                }
              });
            }
          }
        }
      );
    }
  }
};

module.exports = changeLoadState;

const LoadModel = require('../../models/LoadModel');
const TruckModel = require('../../models/TruckModel');
const UserModel = require('../../models/UserModel');
const jwt = require('jsonwebtoken');

const TRUCK_TYPES = {
  SPRINTER: { width: 300, length: 250, height: 170, payload: 1700 },
  'SMALL STRAIGHT': { width: 500, length: 250, height: 170, payload: 2500 },
  'LARGE STRAIGHT': { width: 700, length: 350, height: 200, payload: 4000 },
};
const dimensions = ['width', 'length', 'height'];

const postLoad = (req, res) => {
  const { id } = req.params;
  const [, token] = req.headers['authorization'].split(' ');
  if (!token) {
    res.status(400).json({ message: 'Error occured' });
  } else {
    const myData = jwt.verify(token, 'secret');
    if (myData.role !== 'SHIPPER') {
      res.status(400).json({ message: 'Error occured' });
    } else {
      LoadModel.findOne(
        {
          _id: id,
          created_by: myData._id,
        },
        (err, load) => {
          if (err) {
            res.status(500).json({ message: 'Error occured' });
          } else {
            if (!load) {
              
              res.status(400).json({ message: 'Error occured' });
            } else if (load.status !== 'NEW') {
              res.status(400).json({ message: 'Error occured' });
            } else {
              load.status = 'POSTED';
              load.logs.push({
                message: 'Load posted',
                time: new Date(),
              });
              load.save((err, load) => {
                if (err) res.status(500).json({ message: 'Error occured' });
                else {
                  TruckModel.find({ status: 'IS' }, (err, trucks) => {
                    if (err) res.status(500).json({ message: 'Error occured' });
                    else {
                      let truck = null;
                      for (let i = 0; i < trucks.length; i++) {
                        let isAppropriateTruck = true;
                        dimensions.forEach((dimension) => {
                          if (
                            TRUCK_TYPES[trucks[i].type][dimension] <
                            load.dimensions[dimension]
                          ) {
                            isAppropriateTruck = false;
                          }
                        });
                        if (isAppropriateTruck && trucks[i].assigned_to) {
                          truck = trucks[i];
                          break;
                        }
                      }
                      if (truck) {
                        LoadModel.findOne({ _id: id }, (err, load) => {
                          if (err)
                            res.status(500).json({ message: 'Error occured' });
                          else {
                            load.assigned_to = truck.created_by;
                            load.status = 'ASSIGNED';
                            load.state = 'En route to Pick Up';
                            load.logs.push({
                              message: `Load assigned to driver with id ${truck.created_by}`,
                              time: new Date(),
                            });
                            load.save((err) => {
                              if (err)
                                res
                                  .status(500)
                                  .json({ message: 'Error occured' });
                              else {
                                TruckModel.findOneAndUpdate(
                                  { _id: truck._id },
                                  {
                                    status: 'OL',
                                  }
                                );
                            
                                res.status(200).json({
                                  message: 'Load posted successfully',
                                  driver_found: true,
                                });
                              }
                            });
                          }
                        });
                      } else {
                     
                        LoadModel.findOne({ _id: id }, (err, load) => {
                          if (err)
                            res.status(500).json({ message: 'Error occured' });
                          else {
                            load.status = 'NEW';
                            load.logs.push({
                              message: `Changing status to 'NEW'`,
                              time: new Date(),
                            });
                            load.save((err) => {
                              if (err)
                                res
                                  .status(500)
                                  .json({ message: 'Error occured' });
                              else {
                                res
                                  .status(400)
                                  .json({ message: 'Error occured' });
                              }
                            });
                          }
                        });
                      }
                    }
                  });
                }
              });
            }
          }
        }
      );
    }
  }
};

module.exports = postLoad;
